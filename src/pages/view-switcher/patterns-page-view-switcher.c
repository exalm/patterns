/*
 * Copyright (C) 2021 Marco Melorio <marco.melorio@protonmail.com>
 *
 * SPDX-License-Identifier: MIT or LGPL-2.1-or-later
 */

#include "patterns-config.h"
#include "patterns-page-view-switcher.h"

#include "patterns-utils.h"

struct _PatternsPageViewSwitcher
{
  AdwNavigationPage parent_instance;

  AdwDialog *demo;
};

G_DEFINE_TYPE (PatternsPageViewSwitcher, patterns_page_view_switcher, ADW_TYPE_NAVIGATION_PAGE)

static void
demo_activate_cb (PatternsPageViewSwitcher *self,
                  const char               *action_name,
                  GVariant                 *parameter)
{
  adw_dialog_present (self->demo, GTK_WIDGET (self));
}

static void
patterns_page_view_switcher_class_init (PatternsPageViewSwitcherClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_install_action (widget_class, "demo.run", NULL,
                                   (GtkWidgetActionActivateFunc) demo_activate_cb);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Patterns/pages/view-switcher/patterns-page-view-switcher.ui");
  gtk_widget_class_bind_template_child (widget_class, PatternsPageViewSwitcher, demo);
}

static void
patterns_page_view_switcher_init (PatternsPageViewSwitcher *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
