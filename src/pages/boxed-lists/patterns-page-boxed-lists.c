/*
 * Copyright (C) 2021 Purism SPC
 * Copyright (C) 2021 Marco Melorio <marco.melorio@protonmail.com>
 *
 * SPDX-License-Identifier: MIT or LGPL-2.1-or-later
 */

#include "patterns-config.h"
#include "patterns-page-boxed-lists.h"

#include <glib/gi18n.h>

#include "patterns-utils.h"

struct _PatternsPageBoxedLists
{
  AdwNavigationPage parent_instance;

  AdwDialog *sub_view;
};

G_DEFINE_TYPE (PatternsPageBoxedLists, patterns_page_boxed_lists, ADW_TYPE_NAVIGATION_PAGE)

static void
sub_view_activate_cb (PatternsPageBoxedLists *self,
                      const char             *action_name,
                      GVariant               *parameter)
{
  adw_dialog_present (self->sub_view, GTK_WIDGET (self));
}

static void
external_link_activate_cb (PatternsPageBoxedLists *self,
                           const char             *action_name,
                           GVariant               *parameter)
{
  GtkWindow *window = GTK_WINDOW (gtk_widget_get_root (GTK_WIDGET (self)));
  GtkUriLauncher *launcher;

  /* Translators: must be a valid URI for an external application */
  launcher = gtk_uri_launcher_new (_("https://os.gnome.org"));

  gtk_uri_launcher_launch (launcher, window, NULL, NULL, NULL);
}

static void
patterns_page_boxed_lists_class_init (PatternsPageBoxedListsClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_install_action (widget_class, "page.sub-view", NULL,
                                   (GtkWidgetActionActivateFunc) sub_view_activate_cb);
  gtk_widget_class_install_action (widget_class, "page.external-link", NULL,
                                   (GtkWidgetActionActivateFunc) external_link_activate_cb);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Patterns/pages/boxed-lists/patterns-page-boxed-lists.ui");
  gtk_widget_class_bind_template_child (widget_class, PatternsPageBoxedLists, sub_view);
}

static void
patterns_page_boxed_lists_init (PatternsPageBoxedLists *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
