/*
 * Copyright (C) 2021 Purism SPC
 *
 * SPDX-License-Identifier: MIT or LGPL-2.1-or-later
 */

#include "patterns-config.h"
#include "patterns-primary-window.h"

#include <adwaita.h>
#include <glib/gi18n.h>

#include "patterns-utils.h"

struct _PatternsPrimaryWindow
{
  AdwWindow parent_instance;

  AdwDialog *secondary_window;
  AdwDialog *preferences_window;
};

G_DEFINE_TYPE (PatternsPrimaryWindow, patterns_primary_window, ADW_TYPE_WINDOW)

static void
secondary_activate_cb (PatternsPrimaryWindow *self,
                       const char            *action_name,
                       GVariant              *parameter)
{
  adw_dialog_present (self->secondary_window, GTK_WIDGET (self));
}

static void
preferences_activate_cb (PatternsPrimaryWindow *self,
                         const char            *action_name,
                         GVariant              *parameter)
{
  adw_dialog_present (self->preferences_window, GTK_WIDGET (self));
}

static void
about_activate_cb (PatternsPrimaryWindow *self,
                   const char            *action_name,
                   GVariant              *parameter)
{
  AdwAboutDialog *about;

  const char *developers[] = {
    "Angela Avery <angela@example.org>",
    NULL
  };

  const char *artists[] = {
    "GNOME Design Team",
    NULL
 };

  const char *special_thanks[] = {
    "My cat",
    NULL
 };

  const char *release_notes = "\
<p>\
  This release adds the following features:\
</p>\
<ul>\
  <li>Added a way to export fonts.</li>\
  <li>Better support for <code>monospace</code> fonts.</li>\
  <li>Added a way to preview <em>italic</em> text.</li>\
  <li>Bug fixes and performance improvements.</li>\
  <li>Translation updates.</li>\
</ul>\
  ";

  about =
    g_object_new (ADW_TYPE_ABOUT_DIALOG,
                  "application-icon", "org.example.Typeset",
                  "application-name", _("Typeset"),
                  "developer-name", _("Angela Avery"),
                  "version", "1.2.3",
                  "release-notes-version", "1.2.0",
                  "release-notes", release_notes,
                  "comments", _("Typeset is an app that doesn’t exist and is used as an example content for this about window."),
                  "website", "https://example.org",
                  "issue-url", "https://example.org",
                  "support-url", "https://example.org",
                  "copyright", "© 2022 Angela Avery",
                  "license-type", GTK_LICENSE_LGPL_2_1,
                  "developers", developers,
                  "artists", artists,
                  "translator-credits", _("translator-credits"),
                  NULL);

  adw_about_dialog_add_link (about,
                             _("_Documentation"),
                             "https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.AboutWindow.html");

  adw_about_dialog_add_legal_section (about,
                                      _("Fonts"),
                                      NULL,
                                      GTK_LICENSE_CUSTOM,
                                      "This application uses font data from <a href='https://example.org'>somewhere</a>.");

  adw_about_dialog_add_acknowledgement_section (about,
                                                _("Special thanks to"),
                                                special_thanks);

  adw_dialog_present (ADW_DIALOG (about), GTK_WIDGET (self));
}

static void
patterns_primary_window_class_init (PatternsPrimaryWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_install_action (widget_class, "demo.secondary", NULL,
                                   (GtkWidgetActionActivateFunc) secondary_activate_cb);
  gtk_widget_class_install_action (widget_class, "demo.preferences", NULL,
                                   (GtkWidgetActionActivateFunc) preferences_activate_cb);
  gtk_widget_class_install_action (widget_class, "demo.about", NULL,
                                   (GtkWidgetActionActivateFunc) about_activate_cb);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Patterns/pages/windows/patterns-primary-window.ui");
  gtk_widget_class_bind_template_child (widget_class, PatternsPrimaryWindow, secondary_window);
  gtk_widget_class_bind_template_child (widget_class, PatternsPrimaryWindow, preferences_window);
}

static void
patterns_primary_window_init (PatternsPrimaryWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
